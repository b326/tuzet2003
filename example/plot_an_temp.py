"""
Dependency of An on leaf temperature
====================================

Plot light response curve for both t_leaf=cte and t_leaf=f(ppfd)
"""
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from tuzet2003 import external, leaf

params = leaf.Params()
rd = 0.82e-6  # [mol CO2.m-2.s-1] from Harley 1992 table 1

lambda_v = external.water_latent_heat_vaporization * 1e3  # [kJ.kg-1] to [J.kg-1]
rho_v = external.water_liquid_density * 1e3  # [kg.l-1] to [kg.m-3]

# compute gas exchange
t_atm = external.kelvin(28)  # [K]
t_soil = external.kelvin(30)  # [K]
rh = 0.8  # [-]
ws = 1  # [m.s-1]
psi_soil = -0.03  # [MPa]

rbv = leaf.bulk_resistance(ws, params)

records = []
for ppfd in np.linspace(0, 2000, 100):  # [µmol photon.m-2.s-1]
    an_tfix, ci_tfix, g_co2_tfix, psi_leaf_tfix, transpi_tfix = leaf.gas_exchange_alt(ppfd,
                                                                                      t_atm,
                                                                                      rh,
                                                                                      t_atm,  # fix t_leaf to t_atm
                                                                                      psi_soil,
                                                                                      rbv,
                                                                                      rd,
                                                                                      params.ca * 0.7,
                                                                                      params)

    an, ci, g_co2, psi_leaf, transpi, t_leaf = leaf.energy_balance(ppfd,
                                                                   t_atm,
                                                                   rh,
                                                                   t_soil,
                                                                   psi_soil,
                                                                   rbv,
                                                                   rd,
                                                                   t_leaf_ini=t_atm,
                                                                   ci_ini=params.ca * 0.7,
                                                                   alpha_soil=0.,
                                                                   params=params)

    records.append(dict(
        ppfd=ppfd,
        an=an,
        an_tfix=an_tfix,
        g_co2=g_co2_tfix,
        ci=ci_tfix,
        psi_leaf=psi_leaf_tfix,
        lev=transpi * rho_v * lambda_v,
        lev_tfix=transpi_tfix * rho_v * lambda_v,
        t_leaf=external.celsius(t_leaf),
    ))

df = pd.DataFrame(records).set_index('ppfd')

# plot result
fig, axes = plt.subplots(3, 1, sharex='all', figsize=(10, 9), squeeze=False)
ax = axes[0, 0]
ax.plot(df.index, df['an'] * 1e6, label=f"t_leaf=f(ppfd)")
ax.plot(df.index, df['an_tfix'] * 1e6, label=f"t_leaf={external.celsius(t_atm):.0f} [°C] fixed")
ax.legend(loc='upper left')
ax.set_ylabel("An [µmol CO2.m-2.s-1]")

ax = axes[1, 0]
ax.plot(df.index, df['t_leaf'])
ax.axhline(y=external.celsius(t_atm), ls='--', color='#aaaaaa')
ax.set_ylabel("t_leaf [°C]")

ax = axes[2, 0]
ax.plot(df.index, df['lev'], label=f"t_leaf=f(ppfd)")
ax.plot(df.index, df['lev_tfix'], label=f"t_leaf={external.celsius(t_atm):.0f} [°C] fixed")
ax.legend(loc='upper left')
ax.set_ylabel("LE [W.m-2]")

ax.set_xlabel("ppfd [µmol photon.m-2.s-1]")

fig.tight_layout()
plt.show()
