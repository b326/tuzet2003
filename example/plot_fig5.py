"""
Figure 5
========

Daily evolution

.. warning::

    unable to reproduce fig5 using formalisms from article

"""
import matplotlib.pyplot as plt
import pandas as pd

from tuzet2003 import external, leaf, pth_clean

# read meas
meas = pd.read_csv(pth_clean / "fig5.csv", sep=";", comment="#", index_col=['hour'])

# read params
weather = pd.read_csv(pth_clean / "fig2.csv", sep=";", comment="#", index_col=['hour'])

params = leaf.Params()
rd = 0.82e-6  # [mol CO2.m-2.s-1] from Harley 1992 table 1

lambda_v = external.water_latent_heat_vaporization * 1e3  # [kJ.kg-1] to [J.kg-1]
rho_v = external.water_liquid_density * 1e3  # [kg.l-1] to [kg.m-3]

# compute daily evolution
psi_soil = -0.03  # TODO soil evolution at some point
ci = params.ca * 0.7  # initial guess

records = []
for hour, row in weather.iterrows():
    t_atm = external.kelvin(row['ta'])
    ppfd = external.rg_to_ppfd(row['rg'])  # [µmol photon.m-2.s-1]
    rh = external.vap_sat(min(row['ta'], row['tr'])) / external.vap_sat(row['ta'])
    t_soil = t_atm
    rbv = leaf.bulk_resistance(row['ws'], params)

    an, ci, g_co2, psi_leaf, transpi, t_leaf = leaf.energy_balance(ppfd,
                                                                   t_atm,
                                                                   rh,
                                                                   t_soil,
                                                                   psi_soil,
                                                                   rbv,
                                                                   rd,
                                                                   t_leaf_ini=t_atm,
                                                                   ci_ini=params.ca * 0.7,
                                                                   alpha_soil=0.,
                                                                   params=params)

    records.append(dict(
        hour=hour,
        an=an,
        ci=ci,
        g_co2=g_co2,
        lev=transpi * rho_v * lambda_v,
    ))

df = pd.DataFrame(records).set_index('hour')

# plot result
fig, axes = plt.subplots(2, 2, figsize=(12, 8), squeeze=False)

ax = axes[0, 0]
ax.plot(meas.index, meas['gs_01'], label="1")
ax.plot(df.index, df['g_co2'], label="sim")

ax.legend(loc='upper left')

ax = axes[0, 1]
ax.plot(meas.index, meas['le_01'], label="1")
ax.plot(df.index, df['lev'], label="sim (leaf level)")

ax.legend(loc='upper left')

ax = axes[1, 0]
ax.plot(meas.index, meas['an_01'], label="1")
ax.plot(df.index, (df['an'] + rd) * 1e6, label="sim (leaf level)")

ax.legend(loc='upper left')

ax = axes[1, 1]
ax.plot(df.index, df['ci'] / params.ca, label="ci/ca")

ax.legend(loc='upper left')

fig.tight_layout()
plt.show()
