"""
Equation A12, Soil respiration
==============================

Soil respiration with temperature.
"""
import matplotlib.pyplot as plt
import numpy as np

from tuzet2003 import appendix, external

f0_soil = 3e-6  # [mol CO2.m-2.s-1]
ea_soil = 53000  # [J.mol-1]

ts = np.linspace(0, 45, 100)

# plot result
fig, axes = plt.subplots(1, 1, figsize=(10, 8), squeeze=False)
ax = axes[0, 0]
ax.plot(ts, [appendix.eq_a12(external.kelvin(t_soil), f0_soil, ea_soil) for t_soil in ts])

ax.set_xlabel("Soil temperature [°C]")
ax.set_ylabel("Soil respiration [mol CO2.m-2.s-1]")

fig.tight_layout()
plt.show()
