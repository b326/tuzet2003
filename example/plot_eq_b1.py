"""
Equation B1, Irradiance dependency of J
=======================================

Evolution of J with irradiance
"""
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from tuzet2003 import appendix, pth_clean

# read params
tab3 = pd.read_csv(pth_clean / "table3.csv", sep=";", comment="#", index_col=['name'])
pval = tab3['value'].to_dict()

vl_max_ref = pval['vl_max_ref']
j_max_ref = vl_max_ref * 2.  # from text p1114 (Leuning 1997)
jl_max = j_max_ref

alpha = pval['alpha']
theta = pval['theta']


def jval(ppfd):
    poly = np.polynomial.Polynomial([alpha * ppfd * jl_max, -(alpha * ppfd + jl_max), theta])
    return min(poly.roots())


ppfds = np.linspace(0, 2000, 100)  # [µmol photon.m-2.s-1]

# plot result
fig, axes = plt.subplots(1, 1, figsize=(8, 6), squeeze=False)
ax = axes[0, 0]
ax.plot(ppfds, [appendix.eq_b1(ppfd * 1e-6, jl_max, alpha, theta) * 1e6 for ppfd in ppfds], label="algeb")
ax.plot(ppfds, [jval(ppfd * 1e-6) * 1e6 for ppfd in ppfds], '--', label="np.roots")

ax.legend(loc='lower right')
ax.set_xlabel("ppfd [µmol photon.m-2.s-1]")
ax.set_ylabel("J [µmol CO2.m-2.s-1]")

fig.tight_layout()
plt.show()
