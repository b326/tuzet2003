"""
Equation B2, Temperature dependency of Kc and Ko
================================================

Evolution of Kc and Ko with temperature
"""
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from tuzet2003 import appendix, external, pth_clean

# read params
tab3 = pd.read_csv(pth_clean / "table3.csv", sep=";", comment="#", index_col=['name'])
pval = tab3['value'].to_dict()

t_ref = pval['t_ref']
kc_ref = pval['kc_ref']
ko_ref = pval['ko_ref']
ea_c = pval['ec']
ea_o = pval['eo']

ts = np.linspace(0, 45, 100)

# plot result
fig, axes = plt.subplots(1, 2, sharex='all', figsize=(10, 6), squeeze=False)
ax = axes[0, 0]
ax.plot(ts, [appendix.eq_b2(external.kelvin(t_leaf), kc_ref, ea_c, t_ref) for t_leaf in ts])

ax.set_xlabel("Leaf temperature [°C]")
ax.set_ylabel("Kc [mol.mol-1]")

ax = axes[0, 1]
ax.plot(ts, [appendix.eq_b2(external.kelvin(t_leaf), ko_ref, ea_o, t_ref) for t_leaf in ts])

ax.set_xlabel("Leaf temperature [°C]")
ax.set_ylabel("Ko [mol.mol-1]")

fig.tight_layout()
plt.show()
