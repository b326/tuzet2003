"""
Figure 6a
=========

Sensitivity of stomata to leaf water potential.
"""
import matplotlib.pyplot as plt
import numpy as np

from tuzet2003 import raw

# params from legend
params = dict(
    i=dict(s_f=4.9, psi_f=-1.2),
    ii=dict(s_f=3.2, psi_f=-1.9),
    iii=dict(s_f=2.3, psi_f=-2.6)
)

psis = np.linspace(-5, 0, 100)

# plot
fig, axes = plt.subplots(1, 1, figsize=(8, 6), squeeze=False)
ax = axes[0, 0]

for mod, pm in sorted(params.items(), reverse=True):
    ax.plot(psis, [raw.eq2(psi, pm['psi_f'], pm['s_f']) for psi in psis], label=mod)

ax.legend(loc='upper left')
ax.set_xlim(-5, 0)
ax.set_xlabel("psi_v [MPa]")
ax.set_ylim(0, 1)
ax.set_ylabel("f_psi_v [-]")

fig.tight_layout()
plt.show()
