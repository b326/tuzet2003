"""
Equation B4, Temperature dependency of gamma_star
=================================================

Evolution of gamma_star with temperature
"""
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from tuzet2003 import appendix, external, pth_clean

# read params
tab3 = pd.read_csv(pth_clean / "table3.csv", sep=";", comment="#", index_col=['name'])
pval = tab3['value'].to_dict()

t_ref = pval['t_ref']
gamma_0 = pval['gamma_0']
gamma_1 = pval['gamma_1']
gamma_2 = pval['gamma_2']

ts = np.linspace(0, 45, 100)

# plot result
fig, axes = plt.subplots(1, 1, figsize=(6, 6), squeeze=False)
ax = axes[0, 0]
ax.plot(ts, [appendix.eq_b4(external.kelvin(t_leaf), gamma_0, gamma_1, gamma_2, t_ref) for t_leaf in ts])

ax.set_xlabel("Leaf temperature [°C]")
ax.set_ylabel("gamma_star [mol.mol-1]")

fig.tight_layout()
plt.show()
