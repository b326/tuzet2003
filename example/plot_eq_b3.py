"""
Equation B3, Temperature dependency of Vcmax and Jmax
=====================================================

Evolution of Vcmax and Jmax with temperature
"""
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from tuzet2003 import appendix, external, pth_clean

# read params
tab3 = pd.read_csv(pth_clean / "table3.csv", sep=";", comment="#", index_col=['name'])
pval = tab3['value'].to_dict()

t_ref = pval['t_ref']
vl_max_ref = pval['vl_max_ref']
j_max_ref = vl_max_ref * 2.  # from text p1114 (Leuning 1997)
ea_v = pval['ea_v']
ed_v = pval['ed_v']
ea_j = pval['ea_j']
ed_j = pval['ed_j']
s = pval['s']

ts = np.linspace(0, 45, 100)

# plot result
fig, axes = plt.subplots(1, 1, figsize=(10, 6), squeeze=False)
ax = axes[0, 0]
ax.plot(ts, [appendix.eq_b3(external.kelvin(t_leaf), vl_max_ref, ea_v, ed_v, s, t_ref) * 1e6 for t_leaf in ts],
        label="Vcmax")
ax.plot(ts, [appendix.eq_b3(external.kelvin(t_leaf), j_max_ref, ea_j, ed_j, s, t_ref) * 1e6 for t_leaf in ts],
        label="Jmax")

ax.legend(loc='upper left')
ax.set_xlabel("Leaf temperature [°C]")
ax.set_ylabel("[µmol CO2.m-2.s-1]")

fig.tight_layout()
plt.show()
