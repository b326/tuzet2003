"""
Soil water relation
===================

Plot relation between soil water content and potential or conductance.
"""
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from tuzet2003 import pth_clean, raw

tab1 = pd.read_csv(pth_clean / "table1.csv", sep=";", comment="#", index_col=['name'])

# compute retention curves
dfs = []
for texture, df_params in tab1.groupby('texture'):
    params = df_params['value'].to_dict()
    records = []
    for theta in np.linspace(0.05, params['theta_sat'], 100):
        records.append(dict(
            theta=theta,
            psi=raw.eq4a(theta, params['theta_sat'], params['psi_e'], params['b']),
            k=raw.eq4b(theta, params['theta_sat'], params['k_sat'], params['b'])
        ))

    df = pd.DataFrame(records)

    dfs.append([texture, df])

# plot
fig, axes = plt.subplots(1, 2, sharex='all', figsize=(12, 6), squeeze=False)

for texture, df in dfs:
    ax = axes[0, 0]
    ax.plot(df['theta'], df['psi'], label=texture)

    ax = axes[0, 1]
    ax.plot(df['theta'], df['k'] * 1e4, label=texture)

ax = axes[0, 0]
ax.legend(loc='lower right')
ax.set_ylim(-4, 0)
ax.set_ylabel("psi [MPa]")
ax.set_xlabel("theta [m3.m-3]")

ax = axes[0, 1]
ax.legend(loc='upper left')
ax.set_ylabel("K (x1e4) [s-1.MPa-1.m2]")
ax.set_xlabel("theta [m3.m-3]")

fig.tight_layout()
plt.show()
