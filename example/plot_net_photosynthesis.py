"""
Net photosynthesis
==================

Plot light response curve for different leaf temperatures
"""
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from tuzet2003 import external, leaf

params = leaf.Params()
gsws = 1.
rd = 0.82e-6  # [mol CO2.m-2.s-1] from Harley 1992 table 1

# compute photosynthesis
records = []
for t_leaf in (15, 20, 25, 30, 35, 40):  # [°C]
    for ppfd in np.linspace(0, 2000, 100):  # [µmol photon.m-2.s-1]
        an, ci, g_co2 = leaf.photo_net(ppfd, external.kelvin(t_leaf), gsws, rd, params.ca * 0.7, params)

        records.append(dict(
            t_leaf=t_leaf,
            ppfd=ppfd,
            an=an,
            g_co2=g_co2,
            ci=ci
        ))

df = pd.DataFrame(records).set_index('ppfd')

# plot result
fig, axes = plt.subplots(1, 1, figsize=(8, 6), squeeze=False)
ax = axes[0, 0]

for t_leaf, sdf in df.groupby('t_leaf'):
    ax.plot(sdf.index, sdf['an'] * 1e6, label=f"{t_leaf:.0f}")

ax.legend(loc='upper left', title="t_leaf")
ax.set_xlabel("ppfd [µmol photon.m-2.s-1]")
ax.set_ylabel("An [µmol CO2.m-2.s-1]")

fig.tight_layout()
plt.show()
