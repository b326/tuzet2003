"""
Figure 2
========

Plot measures from figure 2
"""
import matplotlib.pyplot as plt
import pandas as pd

from tuzet2003 import pth_clean

# read meas
meas = pd.read_csv(pth_clean / "fig2.csv", sep=";", comment="#", index_col=['hour'])

# plot result
fig, axes = plt.subplots(1, 1, figsize=(10, 8), squeeze=False)

ax = axes[0, 0]
for name in ('rg', 'ra'):
    ax.plot(meas.index, meas[name], label=name)

ax.legend(loc='upper left')
ax.set_xlabel("time [h]")
ax.set_ylabel("rad [W.m-2]")
ax.set_ylim(0, 1000)

ax = ax.twinx()
for name in ('ta', 'tr', 'ws'):
    ax.plot(meas.index, meas[name], '--', label=name)

ax.legend(loc='upper right')
ax.set_ylabel("t [°C] or ws [m.s-1]")
ax.set_ylim(0, 25)

ax.set_xlim(0, 24)
ax.set_xticks(range(0, 25, 6))

fig.tight_layout()
plt.show()
